<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Consulta viviendas</title>
  <meta name="description" content="">
  <meta name="author" content="Alex.Sindiukov">
  <!--<link rel="stylesheet" href="css/styles.css?v=1.0">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>
<?php
	$conexion= new mysqli("localhost","root","","lindavista");
    $results_per_page = 5;
    $datatable = "viviendas";
	/*Comprobar conexión*/
	if($conexion->connect_errno){
		printf("Conexión fallida: %s\n",$conexion->connect_error);
		exit();
	}
	
	$consulta="SELECT tipo, zona, ndormitorios, precio, tamano, extras, foto FROM viviendas"; 
?>
<body>
<header>
    <h2>Consulta de viviendas</h2>
</header>
   
<section>
    <table class="table">
        <tbody>
            <tr>
            <td>Tipo</td>
            <td>Zona</td>
            <td>Dormitorios</td>
            <td>Precio</td>
            <td>Tamaño</td>
            <td>Extras</td>
            <td>Foto</td>
            </tr>
            <tr>

<?php	 if ($resultado=$conexion->query($consulta))
{
		  if ($resultado->num_rows>0)
          {
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
              //echo $page; echo "---\$page--<br>";
$start_from = ($page-1) * $results_per_page;
              //echo $start_from; echo "---\$start_from--<br>";
$sql = "SELECT * FROM ".$datatable." LIMIT $start_from, ".$results_per_page;

            $rs_result = $conexion->query($sql); 
              
            //var_dump ($rs_result); echo "---\$rs_result--<br>";
			while ($fila=$rs_result->fetch_assoc())
            
            { ?>
            
            <?php echo "<tr>"; echo "<td>";echo $fila['tipo'];echo "</td>";
             echo "<td>";echo $fila['zona'];echo "</td>";
             echo "<td>";echo $fila['ndormitorios'];echo "</td>";
             echo "<td>";echo $fila['precio'];echo "</td>";
             echo "<td>";echo $fila['tamano'];echo "</td>";
             echo "<td>"; echo $fila['extras'];echo "</td>";
             echo "<td>";echo "<a href='./fotos/".$fila['foto']."'><img src='./fotos/ico-fichero.gif' />foto</a></td>"; echo "<tr>";                                    
			}
			$resultado->close();
		    }
            else{
			echo "No se encontró ningún registro que coincida con la búsqueda";
		      }
}
else{
echo "ERROR: No fue posible ejecutar $consulta.".$conexion->error;
}
	
?>        
<?php
$sql1 = "SELECT COUNT(ID) AS total FROM ".$datatable; 
$result = $conexion->query($sql1);
// var_dump($result);
$row = $result->fetch_assoc();
$total_pages = ceil($row["total"] / $results_per_page);
?>     
            <td>Tipo</td>
            <td>Zona</td>
            <td>Dormitorios</td>
            <td>Precio</td>
            <td>Tamaño</td>
            <td>Extras</td>
            <td>Foto</td>
            </tr>
        </tbody>
    </table>
    <div style="display:flex;justify-content: space-between;">
            <div>Mostrando viviendas: <?php 
        for ($i=1; $i<=$total_pages; $i++) { 
            echo "<a href='consulta_viviendas.php?page=".$i."'>".$i."</a> "; 
                
        };
        //$conexion->close();
        ?></div>
        
            
            
    </div>
</section>    
<footer>
</footer>
</body>