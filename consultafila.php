﻿<?php
/*Abrir una conexión*/
$conexion= new mysqli("localhost","root","","world");
/*comprobamos la conexion*/
if ($conexion->connect_errno){
	printf("Conexión fallida: %s\n",$conexion->connect_error);
	exit();
}
$consulta="SELECT Name, CountryCode FROM City ORDER BY Name";

if($resultado=$conexion->query($consulta)){
/*saltar a la fila número 400*/
$resultado->data_seek(399);
/*obtener fila*/
$fila=$resultado->fetch_row();
printf("Ciudad: %s <br> Código de país: %s<br>",$fila[0],$fila[1]);
$resultado->close();
}
/*liberar resultados*/
$conexion->close();
?>