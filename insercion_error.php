<?php session_start(); 
$_SESSION['form'] = array($_POST['tipo'], $_POST['direccion'], $_POST['zona'],     $_POST['ndormitorio'],  $_POST['tamano'],  $_POST['precio'], $_POST['extras'], $_FILES['fileToUpload'],  $_POST['observaciones']);

//  var_dump($_SESSION['form']);

 $error='';
    if (empty($_SESSION['form'][1])) { echo $error; $error.='<li>La dirección del piso esta vacío</li>';}
    if (!is_numeric($_SESSION["form"][4])) { $error.='<li>El tamaño de piso no es un número</li>';}
    if (!is_numeric($_SESSION["form"][5])) { $error.='<li>El precio de precio no es un número</li>';}
    
    $file_size=$_FILES['fileToUpload']['size'];
    if ($file_size > 100000) {$error.='<li>El tamaño de foto es màs de 100kb, cambia el imàgen a mas pequeño</li>';}
    else {
        $target_Path = "fotos/";
        $target_Path = $target_Path.basename( $_FILES['fileToUpload']['name'] );
        move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_Path );
    }
    
    if ($error=='') {header('Location:insertar.php');}
?>  
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Insertar de vivienda</title>
  <meta name="description" content="">
  <meta name="author" content="Alex.Sindiukov">
  <!--<link rel="stylesheet" href="css/styles.css?v=1.0">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>
<header></header>
    <section>
        <div class="container">
            <div class="row">
                <div><h2>Inserción de vivienda</h2></div>
                <?php  ?>
                <div>No se ha podido realizar la insercion debido a los siguientes errores:</div>
                <ul>
            <?php echo $error; ?>
                </ul>
            </div>
            <div class="row">
            <div>[ <a href="insertar_vivienda.php">Volver</a> ]</div>
            </div>
        </div>
    </section>
<footer></footer>
<body>
  <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
 