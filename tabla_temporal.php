<?php
	$conexion = new mysqli("localhost","root","","world");
	/*Comprobar conexión*/
	if($conexion->connect_errno){
		printf("Conexión fallida: %s\n",$conexion->connect_error);
		exit();
	}
	/*Crear una tabla que no existe, no devuelve un conjugo de datos*/
	if($conexion->query("CREATE TEMPORARY TABLE myCity LIKE City")===true){
		printf("La tabla myCity se creó de forma satisfactoria.\n");
	}
	/*Las consultas selct devuelven un conjuto de datos que podemos comprobar mediante num_rows*/
	if ($resultado=$conexion->query("SELECT Name FROM City LIMIT 10")){
		printf("%d filas devueltas.\n",$resultado->num_rows);
		/*liberamos el conjunto de resultados*/
		$resultado->close();
	}
	$conexion->close();
?>
	
		