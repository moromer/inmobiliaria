
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Insertar vivienda</title>
  <meta name="description" content="">
  <meta name="author" content="Alex.Sindiukov">
  <!--<link rel="stylesheet" href="css/styles.css?v=1.0">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>
<header>
<div class="container">
            <div class="row">
                <div><h2>Inserción de vivienda</h2></div>
            </div>
        </div>
</header>    <!-- respuesta de formularios       -->
<?php
$conexion= new mysqli("localhost","root","","lindavista");
/*comprobamos la conexion*/
if ($conexion->connect_errno){
	printf("Conexión fallida: %s\n",$conexion->connect_error);
	exit();
}
    $instruccion = "SHOW columns FROM viviendas LIKE 'tipo'";
    $consulta= $conexion->query($instruccion);
    $row= $consulta->fetch_array();

if($consulta){
//pasar valore a una tabla
    $lis = strstr($row[1], "(");
    $lis = ltrim($lis, "(");
    $lis = rtrim($lis, ")");
    $lista=explode(",", $lis);
}
else{
    echo "ERROR:No fue posible ejecutar la petición";
    $conexion->close();
}
?>
<!-- form begins ---------------------------------------------------->
    <section>
        <div class="container">
            <div class="row">
        <form class="form-horizontal" action='insercion_error.php' method="post" enctype="multipart/form-data">
<fieldset>
<!-- Form Name -->
<legend>Introduzca los datos de la vivienda:</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Tipo de vivienda</label>
  <div class="col-md-4">
    <select id="tipo" name="tipo" class="form-control">
<?php //mostrar cada valor en un elemento OPTION
        if (isset($submit))
            $selected = $tipo;
        else
            $selected = "";
        for ($i=0; $i<count($lista); $i++)
        {
            $cad = trim ($lista[$i], "'");
            if ($cad==$selected)
                print ("<OPTION SELECTED>". $cad);
            else print ("<OPTION>". $cad);
        }
?>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="direccion">Dirección</label>  
  <div class="col-md-4">
  <input id="direccion" name="direccion" type="text" class="form-control input-md">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Zona</label>
  <div class="col-md-4">
    <select id="zona" name="zona" class="form-control">
      <option value="1">Centro</option>
      <option value="2">Nervión</option>
      <option value="3">Triana</option>
      <option value="4">Aljarafe</option>
      <option value="5">Macarena</option>
    </select>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Numero de dormitorios:</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="radios-0">
      <input type="radio" name="ndormitorio" id="ndormitorio-0" value="1" checked="checked">
      1
    </label> 
    <label class="radio-inline" for="radios-1">
      <input type="radio" name="ndormitorio" id="ndormitorio-1" value="2">
      2
    </label> 
    <label class="radio-inline" for="radios-2">
      <input type="radio" name="ndormitorio" id="ndormitorio-2" value="3">
      3
    </label> 
    <label class="radio-inline" for="radios-3">
      <input type="radio" name="ndormitorio" id="ndormitorio-3" value="4">
      4
    </label> 
    <label class="radio-inline" for="radios-4">
      <input type="radio" name="ndormitorio" id="ndormitorio-4" value="5">
      5
    </label>
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tamano">Tamaño</label>  
  <div class="col-md-4">
  <input id="tamano" name="tamano" type="text" class="form-control input-md">
  <span class="help-block">metros cuadrados</span>
      
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="precio">Precio</label>  
  <div class="col-md-4">
  <input id="precio" name="precio" type="text" class="form-control input-md">
  <span class="help-block">€</span>  
    
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="checkboxes">Extras (marque los que procedan):</label>
  <div class="col-md-4">
    <label class="checkbox-inline" for="checkboxes-0">
      <input type="checkbox" name="extras[]" value="Piscina">
      Piscina
    </label>
    <label class="checkbox-inline" for="checkboxes-1">
      <input type="checkbox" name="extras[]" value="Jardin" checked>
      Jardin
    </label>
    <label class="checkbox-inline" for="checkboxes-2">
      <input type="checkbox" name="extras[]" value="Garaje">
      Garaje
    </label>
      
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="filebutton">Foto</label>
  <div class="col-md-4">
    <input id="file" name="fileToUpload" class="input-file" type="file" accept="image/*">
     
  </div>
</div>
<!-- /* Upload image into folder fotos    */ -->

    
<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="observaciones">Observaciones</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="observaciones" name="observaciones">Aire condicionado frio/calor, trastero, amueblado, reciente construcción</textarea>
      
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Insertar vivienda</button>
  </div>
</div>

</fieldset>
</form>
            </div></div>
    </section>

<footer></footer>
<body>
 <!-- <script src="js/scripts.js"></script>-->
    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
<?php 
//if (($_POST["direccion"]) ) { //$_SERVER["REQUEST_METHOD"] == "POST"
  if (empty($_POST["direccion"])) {
    $dirErr = "Direccion is required";
    $error=true; 
  }
    if (!is_numeric($_POST['tamano'])) {
        $tamErr = '<li>El tamaño de piso no es un número</li>';
       $error=true; 
    }
    if (!is_numeric($_POST['precio'])) {
        $precioErr = '<li>El precio de precio no es un número</li>';
       $error=true; 
    }   
    $file_size=$_FILES['fileToUpload']['size'];
    if ($file_size > 100000) {$error=true; $fileErr='<li>El tamaño de foto es màs de 100kb, cambia el imàgen a mas pequeño</li>';}
    else {
        $target_Path = "fotos/";
        $target_Path = $target_Path.basename( $_FILES['fileToUpload']['name'] );
        move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_Path );
    }
//        }
    

/*
if (empty($_SESSION['form'][1])) { echo $error; $error.='<li>La dirección del piso esta vacío</li>';}
    if (!is_numeric($_SESSION["form"][4])) { $error.='<li>El tamaño de piso no es un número</li>';}
    if (!is_numeric($_SESSION["form"][5])) { $error.='<li>El precio de precio no es un número</li>';}
    
    $file_size=$_FILES['fileToUpload']['size'];
    if ($file_size > 100000) {$error.='<li>El tamaño de foto es màs de 100kb, cambia el imàgen a mas pequeño</li>';}
    else {
        $target_Path = "fotos/";
        $target_Path = $target_Path.basename( $_FILES['fileToUpload']['name'] );
        move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_Path );
    }
    
    if ($error=='') {header('Location:section1.php');}*/
?>