
<?php session_start();  
?>
<!--
Extras no muestran
-->
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Insertar de vivienda</title>
  <meta name="description" content="">
  <meta name="author" content="Alex.Sindiukov">
  <!--<link rel="stylesheet" href="css/styles.css?v=1.0">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>
<section>
        <div class="container">
            <div class="row">
                <div>Estos son los datos introducidos:</div>
                <ul>
                <li>Tipo: <?php $tipo=$_SESSION['form'][0]; 
                                if ($tipo==1) {
                                    echo "Piso";
                                } elseif ($tipo==2) {
                                    echo "Adosado";
                                } elseif ($tipo==3){
                                    echo "Chalet";
                                } else {
                                    echo "Casa";
                                }
                    ?></li>
                <li>Zona: <?php $zona=$_SESSION['form'][1]; 
                                if     ($zona==1) {echo "Centro";}
                                elseif ($zona==2) {echo "Nervión";}
                                elseif ($zona==3) {echo "Triana";}
                                elseif ($zona==4) {echo "Aljarafe";}
                                else              {echo "Macarena";}
                    ?></li>
                <li>Direccion: <?php echo $_SESSION['form'][2]; ?></li>
                <li>Numero de dormitorios: <?php echo $_SESSION['form'][3]; ?></li>
                <li>Precio: <?php echo $_SESSION['form'][4]; ?></li>
                <li>Tamaño: <?php echo $_SESSION['form'][5]; ?> metros cuadrados</li>
                <li>Extras: <?php if (!empty($_SESSION['form'][6]))
                                    {
                                    foreach($_SESSION['form'][6] as $selected){
                                    echo $selected."; ";
                                    }}
                    ?></li>
                    <li>Foto: <a href="
                        <?php 
                        $target_Path = "fotos/"; if (isset($_SESSION['form'][7])) {echo $target_Path.basename($_SESSION['form'][7]['name']);} 
                        ?> 
                                        ">
                        <?php
                        if (isset($_SESSION['form'][7])) {
                        print($_SESSION['form'][7]['name']);
                        $target_Path = "fotos/";
                        $target_Path = $target_Path.basename( $_SESSION['form'][7]['name'] );
                            
                        move_uploaded_file($_SESSION['form'][7]['tmp_name'], $target_Path );}
                        ?>
                        </a>
                    </li>
                <li>Observaciones: <?php echo $_SESSION["form"][8]; ?></li>
                </ul>
            </div>
            <div class="row">
            <div></div>
            </div>
            <div class="row">
            <div>[ <a href="insertar_vivienda.html">insertar otra vivienda</a> ]</div>
            </div>
        </div>
</section>
</html>