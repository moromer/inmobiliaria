﻿<?php
/*Abrir una conexión*/
$conexion= new mysqli("localhost","root","","world");
/*comprobamos la conexion*/
if ($conexion->connect_errno){
	printf("Conexión fallida: %s\n",$conexion->connect_error);
	exit();
}
$consulta="SELECT Name, SurfaceArea FROM Country ORDER BY Name LIMIT 5";
if($resultado=$conexion->query($consulta)){
/*Obtener información del campo para la columna SurfaceArea*/
$info_campo=$resultado->fetch_field_direct(1);
printf("Nombre:  %s<br>",$info_campo->name);
printf("Tabla:  %s<br>",$info_campo->table);
printf("Longitud máxima:  %s<br>",$info_campo->max_length);
printf("Banderas:  %s<br>",$info_campo->flags);
printf("Tipo:  %s<br>",$info_campo->type);
$resultado->close();
}
$conexion->close();
?>