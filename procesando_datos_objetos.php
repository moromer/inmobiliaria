<?php
	$conexion = new mysqli("localhost","root","","world");
	/*Comprobar conexión*/
	if($conexion->connect_errno){
		printf("Conexión fallida: %s\n",$conexion->connect_error);
		exit();
	}

	$consulta="SELECT Name, CountryCode FROM City ORDER BY ID DESC LIMIT 50, 5";
	if ($resultado=$conexion->query($consulta)){
		/*obtener el array de objetos*/
		while ($obj=$resultado->fetch_object()){
			printf("%s (%s)\n", $obj->Name, $obj->CountryCode);
		}
		//liberar el conjunto de resultados
		$resultado->close();
	}else{
		echo "ERROR: No fue posible ejecutar $consulta.".$conexion->error;
	}
	//cierra conexión
	$conexion->close();
?>
	
		