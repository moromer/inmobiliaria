<?php
	$conexion = new mysqli("localhost","root","","world");
	/*Comprobar conexión*/
	if($conexion->connect_errno){
		printf("Conexión fallida: %s\n",$conexion->connect_error);
		exit();
	}

	//intenta ejecutar consulta
	// itera sobre la colección de resultados
	//muestra cada registro y sus campos
	$consulta="SELECT Name, Continent FROM country LIMIT 0,10";
	if ($resultado=$conexion->query($consulta)){
		if ($resultado->num_rows>0){
			while ($fila=$resultado->fetch_array()){
				echo $fila["Name"]."-->".$fila['Continent']."<br>\n";
			}
			$resultado->close();
		}else{
			echo "No se encontró ningún registro que coincida con la búsqueda";
		}
	}else{
		echo "ERROR: No fue posible ejecutar $consulta.".$conexion->error;
	}
	//cierra conexión
	$conexion->close();
?>
	
		