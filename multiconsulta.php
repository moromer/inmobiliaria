<?php
$conexion = new mysqli("localhost", "root", "", "world");

/* comprobar conexión */
if ($conexion->connect_errno) {
    printf("Conexión fallida: %s\n", $conexion->connect_error);
    exit();
}

$query  = "SELECT CURRENT_USER();";
$query .= "SELECT Name FROM City ORDER BY ID LIMIT 20, 5";

/* ejecutar multi consulta */
if ($conexion->multi_query($query)) {
    do {
        /* almacenar primer juego de resultados */
        if ($result = $conexion->store_result()) {
            while ($row = $result->fetch_row()) {
                printf("%s\n", $row[0]);
            }
            $result->close();
        }
        /* mostrar divisor */
        if ($conexion->more_results()) {
            printf("<br>-----------------\n<br>");
        }
    } while ($conexion->next_result());
	//$result->close();
}

/* cerrar conexión */
$conexion->close();
?>