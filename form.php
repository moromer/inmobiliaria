<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Insertar vivienda</title>
  <meta name="description" content="">
  <meta name="author" content="Alex.Sindiukov">
  <link rel="stylesheet" href="css/styles.css?v=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>
<header>
<div class="container">
            <div class="row">
                <div><h2>Inserción de vivienda</h2></div>
            </div>
            <div class="row">
            <div>[ <a href="insertar_vivienda.html">insertar otra vivienda</a> ]</div>
            </div>
        </div>
</header>
    
<footer></footer>
<body>
  <script src="js/scripts.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>